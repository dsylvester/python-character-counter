# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "davis.sylvester"
__date__ = "$Mar 9, 2015 Mar 9, 2015 3:09:40 PM$"


from flask import Flask
from flask import render_template
from flask import request


app = Flask(__name__)
app.debug = True

@app.route('/')
def my_form():
    return render_template("Index.html")

@app.route('/', methods=['POST'])
def my_form_post():
    text = request.form['incomingText']
    myList = list(text)
    myDict = {}  
    
    zz = ''
    
    for x in myList:
        if x in myDict.keys():
            prevValue = myDict.get(x)
            currValue = int(prevValue) + 1
            myDict[x] = currValue
            
        else:
            myDict[x] = 1
            
    
    for p in sorted(myDict):
        zz = zz + p + " = " + str(myDict[p]) + "<br>"
    
    return zz + "<br> Original String was: <br><strong>" + text + '</strong>'

    
if __name__ == "__main__":
    app.run(port=5002)
